# Welcome to the allmighty *Sorcery Commands*, the successor of the **EVEN MIGHTIER** `MAGIC_COMMANDS.md`.
Here you'll find useful stuff from Linux, bash-scripts, git, python, your mothe... well, you get the message.

## Linux 'n' chill
-`!command` in shell does what the previous instance of that command did.
    - for example: if you called `git status` in the previous use of the command git, by calling `!git` you call `git status` again. Learn more [here](https://stackoverflow.com/questions/48748030/in-bash-what-does-exclamation-mark-before-command-means).


## Bash 'n' chill
- creating a shell script: use f.e. text minimalist text editor *nano* `nano your_script_name.sh`, write what you need, after that call `chmod +x` to make the file e**x**ecutable