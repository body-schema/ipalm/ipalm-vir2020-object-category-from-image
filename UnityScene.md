## Soft sponges, dice, cubes in Unity

Site: https://github.com/Hartvi/ipalm-sponges
Unity 2019.4.15f1 (64-bit) + HDRP

Sponges
- How many images? - Michal said it would be nice to have 1500
- Sponges - yes, dice - yes, kinova/other cubes - brown cube
  - Box, pill, cylinder

### Done
- [Google Drive folder](https://drive.google.com/drive/u/1/folders/1ujW8lD5FBNhz46CSuK7PueUcd8TJ1oqa)
- 5 new categories for sponges and dice:
  - 1. "box - foam" 2. "pill - foam" 3. "cylinder - foam" 4. "dice - foam" 5. "dice - soft plastic" 

## TODO
- [ ] - Generate *normal maps* to be be centered around #8080FF RGB=(0.5,0.5,1)
- [ ] - Generate *mask maps* for smoothness of surface.
- [ ] - Add options to chose which encoding to generate
- [ ] - Add partial transparency, for light to come through sponges.
- [ ] - More model shapes & lighting
- [X] - Raycasting from camera instead of visual segmentation.
- [X] - Saving segmentation mask in JSON format using LRE encoding.
- [x] - Create model of die, brown cube, sponges ~8
  - [X] - Models of kinova cubes and blue cube - done brown cube, dice
- [X] - Automate scene creation with combinations of sponges/dice/cubes from all angles
- [X] - Find out how to effectively **map textures** to objects - unnecessary
  - [X] *Take into account* how large things are in real life, e.g. sponge holes have a constant size whereas sponge size increases
- [X] Unity `CaptureScreenshot` has width 1 pixel smaller than `Screen.width`
  - [Getting original size](https://forum.unity.com/threads/getting-original-size-of-texture-asset-in-pixels.165295/)
  - *Found out*: C++++'s System.Drawing and OS and Unity don't like each other
- [X] Binary mask rough around the bottom but smooth on top even though it should be the same ??
  - *Solution* can be increasing vertical probing resolution, e.g. [9,6] => [9,3]
  - OR (not) Learn [HLSL](https://docs.unity3d.com/Manual/SL-ShadingLanguage.html) (Shader Language for GPU)
- [X] RLE - sometimes glitches when the objects goes out of view on the right side 
  - Edge case when going to new line => *Solution*: condition
- [X] In extreme cases: When binary mask is *non-convex* & the non-convex corner is near the border between two probed rectangles, the 1's and 0's get flipped
  - Solution?: Off by one in 1/200 cases when sparse sampling. Sample around edges, not center of rectangle.
- [ ] New computer
  - Current one literally blacks out when generating images

```
JSON:
ims:[
    0:{
        width 
        height
        annotations:[
            0:{
                image_id:x
                segmentation:{
                    bbox
                    counts
                    size
                    mask_file
                }
            }
        ]
        
    }
]
```
