## Progress Document VIR - 2020 IPALM section

# 01/01/2021 
- 6GB dataset learning from scratch, bigger base learning rate, gamma 0.05
- learning rate decay after 4000 iterations
- total loss under 0.3

Below you can find chronological prgress updates:
# 29/12/2020 meeting w/ *Matej Hoffman*
- Shown progress on first predictions
- discussion upon first draft of presentation, editorial
- consulting the form of presentation

# 24/12/2020
- Resolved the previous error `Loss became infinite or NaN` by extending the dataset to greater size
- implemented custom trainer `MegatronTrainer` (**"NOBODY SUMMONS MEGATRON!"**)
  - uses many custom features, including dataloader, cocoevaluator and so on
- delivered first inference results, stored in `PREDICTIONS/preds_24_12_2020`
- met all of the major-milestones, added new
- met the main goal of this VIR project, new milestones are basically for further work related with IPALM

# 19/12/2020
- *Jiri* managed to create also rounded sponges
- created a custom trainer with the COCOEvaluator (not yet implemented)
- resolved some issues with RLE, *Michal* did some workarounds with transforming mask into the byte array
- Currently have following problems with infinite / NaN losses :( :
```
"Loss became infinite or NaN at iteration={self.iter}!\n"
FloatingPointError: Loss became infinite or NaN at iteration=154!
loss_dict = {'loss_cls': 2.324117422103882, 'loss_box_reg': 0.5348302125930786, 'loss_mask': nan, 'loss_rpn_cls': 0.285099059343338, 'loss_rpn_loc': 0.12363328784704208}
```

# 17/12/2020
- still unresolved problem with RLE
- learning without the segmentation was performed successfully on a miniature dataset
  - this means the dataset pipeline is ok, also SW modules @cantor
  - looking into the segmentation problems this weekend

# 16/12/2020
- currently debugging the first learning sequence
- some problems with incompatibility of our RLE format with the official COCO RLE format seemes to cause the error down the line
  - got a series of errors:
    - `TypeError: 'Default Trainer' object is not callable`
    - `File "pycocotools/_mask.pyx", line 128, in pycocotools._mask._frString TypeError: Expected bytes, got list`  here is [link](https://github.com/facebookresearch/detectron2/issues/844)
    - `TypeError: Expected bytes, got str`
    - ``` 
        File "/home.nfs/kruzland/detectron2/detectron2/data/transforms/transform.py", line 113,
        in apply_image assert img.shape[:2] == (self.h, self.w)
        AttributeError: 'dict' object has no attribute 'shape'   
  - tried to resolve the errors, but I ended up editing detectron2 source-files (i've done a copy of each if edited)                
  - edited: `sftp://cantor.felk.cvut.cz/home.nfs/kruzland/detectron2/detectron2/data/detection_utils`                                  

# 11/12/2020 - Friday meeting w/ *Matej Hoffman*
- cross check of object properties
- chosen R50-FPN (according to **Nazarczuk**)
- functional dataset in detectron2-friendly format (segmentation and BBoxes in the right format) - MetaData is causing some trouble though
- added CGI dice (foam cubes) to balance out the small white solid dice.
- planned first test learning

# 04/12/2020 - Friday meeting w/ *Matej Hoffman*
- 1st working version of detectron2 (using some maskrcnn model) by *Andrej*
- progress in dataset completion - common IDs = (object,material) tuple by *Michal*
- RLE (Run Length Encoding) by *Jiri*, generation of dataset with different lighting (sponges) 
  - will generate multi-instance masks or one object at a time

# 02/12/2020 - Weekly Inside Meeting & Detectron 2
- meeting w/ *Teymur Azatyev*
- **EXTREME** debugging (extreme in a sense, that my laptop almost had the adrenalin thrill from freefall ;) )
    - after triple reainstall with different settings, Teymur advised us to switch to the newer version of the maskrcnn --> [The Detectron 2](https://detectron2.readthedocs.io/index.html) which has a superior indepth documentation and is **not deprecated** i.e. still supported (last commit 25 days ago).
- progress with unifying datasets into a single output lable format (**Michal Pliska**)
- Unity project for sponge image and LRE generation. [link](https://github.com/Hartvi/ipalm-sponges)
  - Example image and encoding [link](https://drive.google.com/drive/u/1/folders/1QBZM8wEnp-cFYjLgso8uT_vFMND8gEq0)

# 01/12/2020
- Implemented direct raycasting and LRE encoding for masks in Unity [example image and LRE mask](https://drive.google.com/drive/folders/1ujW8lD5FBNhz46CSuK7PueUcd8TJ1oqa?usp=sharing)

# 27/11/2020 - Friday meeting w/ *Matej Hoffman*
- [Unity example photos](https://drive.google.com/drive/folders/1ujW8lD5FBNhz46CSuK7PueUcd8TJ1oqa?usp=sharing). Binary or other mask not yet implemented. 


# 25/11/2020 - Weekly Inside Meeting
Redirection of worktasks among team members:
- **Jiri** - creating some virtual instances of dataset
- **Michal** - tutorial on how to edit the dataset for COCO format
- **Andrej** - setting up minimum viable instance of RCNN

# 20/11/2020 - Friday meeting w/ *Matej Hoffman*
- discussed set goals, final functionality
- output will be used for IPALM inference pipeline
- coupling of output (the infered) labels
    - that is not only _"cup"_ or _"sponge"_ but also **"ceramic"** or **polyurethane** e.g. (cup, plastic), (cup, ceramic) and so on

# 18/11/2020 - Weekly Inside Meeting
- Collecting of dataset. Setting milestones, todos 

# 13/11/2020 - Friday Meeting w/ *Matej Hoffman*
- Briefing and discussion about the goals of the semestral work.


# Evaluation, DataLogging and Visualisation
## Evaluation
- While we use `DefaultTrainer` and use dataset in the COCO format, we should also use the `COCOEvaluator` which is set as follows [HERE](https://detectron2.readthedocs.io/tutorials/training.html#trainer-abstraction).
- For a general Evaluator API refer [here](https://detectron2.readthedocs.io/modules/evaluation.html#detectron2.evaluation.DatasetEvaluator).
- Evaluator using TTA more [here](https://machinelearningmastery.com/how-to-use-test-time-augmentation-to-improve-model-performance-for-image-classification/)
  - Test-time augmentation, or TTA for short, is an application of data augmentation to the test dataset. Specifically, it involves creating multiple augmented copies of each image in the test set, having the model make a prediction for each, then returning an ensemble of those predictions.
