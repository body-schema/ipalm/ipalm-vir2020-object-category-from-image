import numpy as np
from PIL import Image as im

#not in use
def mask_to_LRE(image_path):
    mask = np.asarray(im.open(image_path))
    black = 0
    white = 0
    waiting = True
    RLE = []

    for x in range(mask.shape[1]):
        for y in range(mask.shape[0]):
            if waiting:
                if mask[y][x] != 0: #white
                    waiting = False
                    RLE.append(black)
                    black = 0
                    white = 1
                else: #black
                    black +=1    
            else:
                if mask[y][x] == 0:
                    waiting = True
                    RLE.append(white)
                    white = 0
                    black = 1
                else:
                    white +=1
    if white != 0:
        RLE.append(white)
    if black != 0:
        RLE.append(black)

    return RLE

#not in use
def bbox_from_mask(image_path):
    mask = np.asarray(im.open(image_path))
    min_x = float('inf')
    min_y = float('inf')
    max_x = -float('inf')
    max_y = -float('inf')
    for y in range(mask.shape[0]):
        for x in range(mask.shape[1]):
            if mask[y][x] != 0:
                if y < min_y:
                    min_y = y
                if x < min_x:
                    min_x = x
                if y > max_y:
                    max_y = y
                if x > max_x:
                    max_x = x
    return [min_x, min_y, max_x, max_y]

def is_mask_visible(image_path):
    mask = np.asarray(im.open(image_path))
    for row in mask:
        for pix in row:
            if pix != 0:
                return True
    return False
    


#print(bbox_from_mask("/Users/michalpliska/Desktop/train_pbr(YCB)/000000/mask_visib/000000_000016.png"))

        
#some related code               
#a = [[0, 0, 255], [255,0,0], [0, 255, 0]]
#a = np.array(a)
#im.fromarray(a.astype(np.uint8)).show()
