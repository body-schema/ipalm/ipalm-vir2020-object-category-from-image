import json
import torch, torchvision
import detectron2
from detectron2.utils.logger import setup_logger
setup_logger()
import numpy as np
import os, json, cv2, random
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
import cv2
import pickle
from PIL import Image
import shutil



def get_dict():
    with open("DATASET/annotations.data", 'rb') as data:
        data = pickle.load(data)
    return data


shutil.rmtree("SIMPLE_TEST")
os.mkdir("SIMPLE_TEST")

DatasetCatalog.register("DATASET/TRAIN", lambda d: get_dict())
dataset_dicts = get_dict()
for d in random.sample(dataset_dicts, 100):
    print(d["file_name"])
    img = cv2.imread(d["file_name"])
    visualizer = Visualizer(img[:, :, ::-1])
    out = visualizer.draw_dataset_dict(d)
    im = Image.open(d["file_name"])
    im.save("SIMPLE_TEST/"+str(d["image_id"]).replace(".jpg", "")+"-0.jpg", "JPEG")
    out.save("SIMPLE_TEST/"+str(d["image_id"]).replace(".jpg", "")+"-1.jpg")


