import json
import pickle

with open('DICTS/OWN_to_id.json') as OWN_to_id:
    OWN_to_id = json.load(OWN_to_id)

with open('DICTS/id_to_OWN.json') as id_to_OWN:
    id_to_OWN = json.load(id_to_OWN)

for txt in ["train", "val", "test"]:
    with open("DATASET/"+txt+".data", 'rb') as annot:
        annot = pickle.load(annot)
    for image in annot:
        for segmentation in image["annotations"]:
            segmentation["category_id"] = id_to_OWN[str(segmentation["category_id"])]
    with open("DATASET_TMP/"+txt+"-OWN.data", 'wb') as file:
        pickle.dump(annot, file)
