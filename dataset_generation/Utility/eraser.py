import os
import random

path = "/Users/michalpliska/Desktop/[name]/"
destruction_rate = 0

folders = os.listdir(path)
for name in ['.DS_Store', 'calibration.h5', 'masks', 'poses']:
        if name in folders:
            folders.remove(name)
            
for folder in folders:
    print(folder)
    image_names = os.listdir(path+folder+"/")
    for name in ['.DS_Store', 'calibration.h5', 'masks', 'poses']:
        if name in image_names:
            image_names.remove(name)
    image_names.sort()
    
    for image_name in image_names:
        if random.random() < destruction_rate:
            os.remove(path+folder+"/"+image_name)
            os.remove(path+folder+"/masks/"+image_name.replace(".jpg", "_mask.pbm"))