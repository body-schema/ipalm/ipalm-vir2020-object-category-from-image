import json
from openpyxl import load_workbook


path = "Info/dataset_definiton_and_translation.xlsx"
wb = load_workbook(path)
categories = set()

#work on SHOP-VRB
dict_SHOP = {}
sheet = wb[wb.sheetnames[0]]
for i in range(2, sheet.max_row+1):
    key = sheet["A"+str(i)].value + " - " + sheet["B"+str(i)].value
    value = sheet["C"+str(i)].value + " - " + sheet["D"+str(i)].value
    categories.add(value)
    dict_SHOP[key] = value
with open('tmp/DICTS/SHOP_to_OWN.json', 'w') as file:
    json.dump(dict_SHOP, file)

#work on YCB
dict_YCB = {}
sheet = wb[wb.sheetnames[1]]
for i in range(2, sheet.max_row+1):
    key = sheet["B" + str(i)].value
    value = sheet["C"+str(i)].value + " - " + sheet["D"+str(i)].value
    categories.add(value)
    dict_YCB[key] = value

with open('tmp/DICTS/YCB_to_OWN.json', 'w') as file:
    json.dump(dict_YCB, file)

# work on YCB(fel)
dict_YCB_fel = {}
sheet = wb[wb.sheetnames[2]]
for i in range(2, sheet.max_row + 1):
    key = sheet["B" + str(i)].value
    value = sheet["C" + str(i)].value + " - " + sheet["D" + str(i)].value
    categories.add(value)
    dict_YCB_fel[key] = value

with open('tmp/DICTS/YCB(fel)_to_OWN.json', 'w') as file:
    json.dump(dict_YCB_fel, file)


# work on UNITY categories
UNITY_categories = ["box - foam", "cylinder - foam", "pill - foam", "dice - foam", "dice - soft plastic"]
for category in UNITY_categories:
    categories.add(category)

# create summary and own->id & id->own
sheet = wb[wb.sheetnames[-1]]
wb.remove(sheet)

sheet = wb.create_sheet("Own categories")
own_to_id = {}
id_to_own = {}
categories = list(categories)
categories.sort()
sheet["A1"] = "OBJECT & CATEGORY"
sheet["B1"] = "ID"
for tup in enumerate(categories):
    sheet["A"+str(tup[0]+2)] = tup[1]
    sheet["B" + str(tup[0] + 2)] = tup[0]
    own_to_id[tup[1]] = tup[0]
    id_to_own[tup[0]] = tup[1]
with open('tmp/DICTS/OWN_to_id.json', 'w') as file:
    json.dump(own_to_id, file)
with open('tmp/DICTS/id_to_OWN.json', 'w') as file:
    json.dump(id_to_own, file)
wb.save("Info/dataset_definiton_and_translation.xlsx")
















