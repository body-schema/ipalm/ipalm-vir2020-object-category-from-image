from detectron2.structures import BoxMode
from PIL import Image as im
from PIL import ImageOps
import os
import numpy as np
import pycocotools.mask as mask_utils
import json
import pickle
import random


def ycb_main(glob_id, glob_annotation, IMAGES, gen_id=True):
    IMAGES_YCB = IMAGES
    path_to_YCB = "Source datasets/YCB/"
    YCB = os.listdir(path_to_YCB)
    YCB.sort()
    if '.DS_Store' in YCB:
        YCB.remove('.DS_Store')
    with open('tmp/DICTS/YCB_to_OWN.json') as YCB_to_OWN:
        YCB_to_OWN = json.load(YCB_to_OWN)
    with open('tmp/DICTS/OWN_to_id.json') as OWN_to_id:
        OWN_to_id = json.load(OWN_to_id)

    IMAGES_PER_FOLDER = int(IMAGES_YCB / len(YCB) - 1)
    print("Images per folder:", IMAGES_PER_FOLDER)

    for folder in YCB:
        path_to_images = path_to_YCB + folder + "/"
        path_to_masks = path_to_YCB + folder + "/masks/"

        images = os.listdir(path_to_images)
        images.sort()
        for name in ['.DS_Store', 'calibration.h5', 'masks', 'poses']:
            if name in images:
                images.remove(name)

        prop_ratio = IMAGES_PER_FOLDER / len(images)
        print("Folder", folder, "    Probability ratio", prop_ratio)
        for item in images:
            if random.random() > prop_ratio:
                continue
            print(item)
            annot = {}
            image = im.open(path_to_images + item)
            file_name = "DATASET/TRAIN/" + str(glob_id) + ".jpg"
            image.save(file_name, "JPEG")
            annot["file_name"] = file_name
            width, height = image.size
            annot["height"] = height
            annot["width"] = width
            annot["image_id"] = glob_id
            glob_id += 1

            annotation = []
            tmp = {}
            tmp["bbox_mode"] = BoxMode.XYXY_ABS
            if gen_id:
                tmp["category_id"] = OWN_to_id[YCB_to_OWN[folder]]
            else:
                tmp["category_id"] = YCB_to_OWN[folder]
            pil_object = im.open(path_to_masks + item.replace(".jpg", "") + "_mask.pbm")
            pil_object = ImageOps.grayscale(pil_object)
            pil_object = ImageOps.invert(pil_object)
            seg = mask_utils.encode(np.asarray(pil_object, dtype="uint8", order="F"))
            tmp["segmentation"] = seg
            bbox = mask_utils.toBbox(seg)
            bbox = [bbox[0], bbox[1], bbox[0] + bbox[2], bbox[1] + bbox[3]]
            tmp["bbox"] = bbox  # fuk dis šit ajm out, stupid XYWH_ABS not vorkink OMG
            annotation.append(tmp)
            annot["annotations"] = annotation
            glob_annotation.append(annot)

    return glob_id, glob_annotation


RUN = True
if RUN:
    glob_id = 0
    glob_annotation = []
    IMAGES = 200
    (glob_id, glob_annotation) = ycb_main(glob_id, glob_annotation, IMAGES)
    with open('DATASET/annotations.data', 'wb') as file:
        pickle.dump(glob_annotation, file)
