from detectron2.structures import BoxMode
from PIL import Image as im
import os
import json
from Utility import mask_processing
import numpy as np
import pycocotools.mask as mask_utils
import pickle

def ycb_fel_main(glob_id, glob_annotation, IMAGES, gen_id=True):
    path_to_YCBfel = "Source datasets/train_pbr(YCB)/"
    YCB_fel = os.listdir(path_to_YCBfel)
    YCB_fel.sort()
    if '.DS_Store' in YCB_fel:
        YCB_fel.remove('.DS_Store')
    with open('tmp/DICTS/YCB(fel)_to_OWN.json') as YCB_fel_to_OWN:
        YCB_fel_to_OWN = json.load(YCB_fel_to_OWN)
    with open('tmp/DICTS/OWN_to_id.json') as OWN_to_id:
        OWN_to_id = json.load(OWN_to_id)

    IMAGES_PER_FOLDER = int(IMAGES / len(YCB_fel))
    print("Images per folder:", IMAGES_PER_FOLDER)

    for name in YCB_fel:
        current_folder = path_to_YCBfel + name + "/"
        rgbs = os.listdir(current_folder + "rgb/")
        rgbs.sort()
        id_list = []

        with open(current_folder + 'scene_gt.json') as data:
            data = json.load(data)
        for key in data:
            for obj in data[key]:
                id_list.append(obj["obj_id"])

        visible_masks = os.listdir(current_folder + "mask_visib/")
        visible_masks.sort()

        DROP = int(len(rgbs) / IMAGES_PER_FOLDER)
        print("Drop in folder", name, ":", DROP)
        COUNT = IMAGES_PER_FOLDER
        while True:
            if COUNT == 0:
                break
            COUNT -= 1

            annot = {}
            image = im.open(current_folder + "rgb/" + rgbs[0])
            file_name = "DATASET/TRAIN/" + str(glob_id) + ".jpg"
            annot["file_name"] = file_name
            width, height = image.size
            annot["height"] = height
            annot["width"] = width
            annot["image_id"] = glob_id
            image.save(file_name, "JPEG")
            glob_id += 1

            annotation = []
            rgb = rgbs[0].replace(".jpg", "")
            while not visible_masks[0].startswith(rgb):
                visible_masks.pop(0)
                id_list.pop(0)
            while visible_masks[0].startswith(rgb):
                tmp = {}
                mask_path = current_folder + "mask_visib/" + visible_masks[0]
                print(mask_path)
                visible_masks.pop(0)
                id = id_list.pop(0)
                if mask_processing.is_mask_visible(mask_path):
                    tmp["category_id"] = YCB_fel_to_OWN[str(id)]
                    if gen_id:
                        tmp["category_id"] = OWN_to_id[YCB_fel_to_OWN[str(id)]]
                    else:
                        tmp["category_id"] = YCB_fel_to_OWN[str(id)]
                    seg = mask_utils.encode(np.asarray(im.open(mask_path), dtype="uint8", order="F"))
                    tmp["segmentation"] = seg
                    tmp["bbox_mode"] = BoxMode.XYXY_ABS
                    bbox = mask_utils.toBbox(seg)
                    bbox = [bbox[0], bbox[1], bbox[0] + bbox[2], bbox[1] + bbox[3]]
                    tmp["bbox"] = bbox
                    annotation.append(tmp)
                if not visible_masks:
                    break

            annot["annotations"] = annotation
            glob_annotation.append(annot)
            rgbs = rgbs[DROP:]

    return glob_id, glob_annotation


if __name__ == "__main__":
    glob_id = 0
    glob_annotation = []
    IMAGES = 50
    (glob_id, glob_annotation) = ycb_fel_main(glob_id, glob_annotation, IMAGES)
    with open('DATASET/annotations.data', 'wb') as file:
        pickle.dump(glob_annotation, file)