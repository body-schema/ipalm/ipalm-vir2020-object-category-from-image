from .SHOP_main import SHOP_main
from .ycb_main import ycb_main
from .ycb_fel_main import ycb_fel_main
from .UNITY_main import UNITY_main
import pickle
import random
import shutil
import os

import translation

shutil.rmtree("DATASET")
os.mkdir("DATASET")
os.mkdir("DATASET/TRAIN")
os.mkdir("DATASET/VAL")
os.mkdir("DATASET/TEST")

ids = True
glob_id = 0
glob_annotation = []

print("Working on SHOP-VRB")
IMAGES = [3000, 3000, 3000, 10000]
glob_id, glob_annotation = SHOP_main(glob_id, glob_annotation, IMAGES, gen_id=ids)


print("Working on UNITY")
glob_id, glob_annotation = UNITY_main("/Users/michalpliska/Desktop/VIR/UNITY/DICE/", glob_id, glob_annotation, gen_id=ids)
glob_id, glob_annotation = UNITY_main("/Users/michalpliska/Desktop/VIR/UNITY/1400+/", glob_id, glob_annotation, gen_id=ids)

print("Working on YCB")
IMAGES = 20000
glob_id, glob_annotation = ycb_main(glob_id, glob_annotation, IMAGES, gen_id=ids)

print("Working on YCB(fel)")
IMAGES = 5000
glob_id, glob_annotation = ycb_fel_main(glob_id, glob_annotation, IMAGES, gen_id=ids)


random.shuffle(glob_annotation)
train = 0.70
val = 0.10
test = 0.20
i_train = int(len(glob_annotation) * train)
i_val = int(len(glob_annotation) * (train + val))
train = glob_annotation[:i_train]
val = glob_annotation[i_train:i_val]
test = glob_annotation[i_val:]
print(len(train), len(val), len(test))

for image in val:
    old_location = image["file_name"]
    new_location = old_location.replace("DATASET/TRAIN", "DATASET/VAL")
    shutil.move(old_location, new_location)
    image["file_name"] = new_location

for image in test:
    old_location = image["file_name"]
    new_location = old_location.replace("DATASET/TRAIN", "DATASET/TEST")
    shutil.move(old_location, new_location)
    image["file_name"] = new_location

with open('DATASET/train.data', 'wb') as file:
    pickle.dump(train, file)
with open('DATASET/val.data', 'wb') as file:
    pickle.dump(val, file)
with open('DATASET/test.data', 'wb') as file:
    pickle.dump(test, file)
import id_to_OWN
import test
os.mkdir("DATASET/info")
shutil.copy('DICTS/id_to_OWN.json', 'DATASET/info/id_to_OWN.json')
shutil.copy("/Users/michalpliska/Desktop/VIR/dataset_definiton_and_translation.xlsx", 'DATASET/info/dataset_definiton_and_translation.xlsx')
