from detectron2.structures import BoxMode
from PIL import Image as im
import json
import pycocotools.mask as mask_utils
import pickle


def SHOP_main(glob_id, glob_annotation, IMAGES, gen_id=True):
    with open('tmp/DICTS/SHOP_to_OWN.json') as SHOP_to_OWN:
        SHOP_to_OWN = json.load(SHOP_to_OWN)
    with open('tmp/DICTS/OWN_to_id.json') as OWN_to_id:
        OWN_to_id = json.load(OWN_to_id)
    files = [("benchmark", IMAGES[0]), ("val", IMAGES[1]), ("test", IMAGES[2]), ("train", IMAGES[3])]
    for name, num in files:
        path_to_SHOP = "Source datasets/shop_vrb/images/" + name + "/"
        with open('Source datasets/shop_vrb/scenes/SHOP_VRB_' + name + "_scenes.json") as data:
            info = json.load(data)
        COUNT = num
        for image in info["scenes"]:
            if COUNT <= 0:
                break
            COUNT -= 1
            print(image["image_filename"])
            pic = im.open(path_to_SHOP + image["image_filename"])
            file_name = "DATASET/TRAIN/" + str(glob_id) + ".jpg"
            pic = pic.convert("RGB")
            pic.save(file_name, "JPEG")
            width, height = pic.size
            annot = {"file_name": file_name, "height": height, "width": width, "image_id": glob_id}
            glob_id += 1
            annotation = []
            for obj in image["objects"]:
                seg = obj["mask"]
                bbox = mask_utils.toBbox(seg)
                bbox = [bbox[0], bbox[1], bbox[0] + bbox[2], bbox[1] + bbox[3]]

                tmp = {"bbox_mode": BoxMode.XYXY_ABS, "bbox": bbox, "segmentation": obj["mask"]}
                if gen_id:
                    tmp["category_id"] = OWN_to_id[SHOP_to_OWN[obj["name"] + " - " + obj["material"]]]
                else:
                    tmp["category_id"] = SHOP_to_OWN[obj["name"] + " - " + obj["material"]]
                annotation.append(tmp)
            annot["annotations"] = annotation
            glob_annotation.append(annot)

    return glob_id, glob_annotation


if __name__ == "__main__":
    glob_id = 0
    glob_annotation = []
    IMAGES = [100, 100, 100, 100]
    (glob_id, glob_annotation) = SHOP_main(glob_id, glob_annotation, IMAGES, gen_id=False)
    with open('DATASET/annotations.data', 'wb') as file:
        pickle.dump(glob_annotation, file)
