from detectron2.utils.logger import setup_logger
setup_logger()
import os, random
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
import cv2
import pickle
from PIL import Image
import shutil



def get_dict(name):
    with open("tmp/Dicts/" + name + "-OWN.data", 'rb') as data:
        data = pickle.load(data)
    return data


shutil.rmtree("tmp/Test")
os.mkdir("tmp/Test")

DatasetCatalog.register("DATASET/TRAIN", lambda d: get_dict("train"))
dataset_dicts = get_dict("train")
for d in random.sample(dataset_dicts, 100):
    print(d["file_name"])
    img = cv2.imread(d["file_name"])
    visualizer = Visualizer(img[:, :, ::-1])
    out = visualizer.draw_dataset_dict(d)
    im = Image.open(d["file_name"])
    im.save("tmp/Test/"+str(d["image_id"]).replace(".jpg", "")+"-0.jpg", "JPEG")
    out.save("tmp/Test/"+str(d["image_id"]).replace(".jpg", "")+"-1.jpg")

DatasetCatalog.register("DATASET/VAL", lambda d: get_dict("val"))
dataset_dicts = get_dict("val")
for d in random.sample(dataset_dicts, 30):
    print(d["file_name"])
    img = cv2.imread(d["file_name"])
    visualizer = Visualizer(img[:, :, ::-1])
    out = visualizer.draw_dataset_dict(d)
    im = Image.open(d["file_name"])
    im.save("tmp/Test/"+str(d["image_id"]).replace(".jpg", "")+"-0.jpg", "JPEG")
    out.save("tmp/Test/"+str(d["image_id"]).replace(".jpg", "")+"-1.jpg")

DatasetCatalog.register("DATASET/TEST", lambda d: get_dict("test"))
dataset_dicts = get_dict("test")
for d in random.sample(dataset_dicts, 30):
    print(d["file_name"])
    img = cv2.imread(d["file_name"])
    visualizer = Visualizer(img[:, :, ::-1])
    out = visualizer.draw_dataset_dict(d)
    im = Image.open(d["file_name"])
    im.save("tmp/Test/"+str(d["image_id"]).replace(".jpg", "")+"-0.jpg", "JPEG")
    out.save("tmp/Test/"+str(d["image_id"]).replace(".jpg", "")+"-1.jpg")

