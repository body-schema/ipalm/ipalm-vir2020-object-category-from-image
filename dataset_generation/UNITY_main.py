from PIL import Image as im
from PIL import ImageOps
import json
import pickle
import numpy as np
import pycocotools.mask as mask_utils
from detectron2.structures import BoxMode


def UNITY_main(path_to_UNITY, glob_id, glob_annotation, gen_id=True):
    with open(path_to_UNITY + 'lre_data.json') as info:
        info = json.load(info)
    with open('tmp/DICTS/OWN_to_id.json') as OWN_to_id:
        OWN_to_id = json.load(OWN_to_id)

    for scene in info["ims"]:
        print(scene["file_name"])
        annot = {}
        image = im.open(path_to_UNITY + scene["file_name"].replace("Resources/", ""))
        file_name = "DATASET/TRAIN/" + str(glob_id) + ".jpg"
        annot["file_name"] = file_name
        width, height = image.size
        annot["height"] = height
        annot["width"] = width
        annot["image_id"] = glob_id
        image.save(file_name, "JPEG")
        glob_id += 1

        annotation = []
        for obj in scene["annotations"]:
            tmp = {}
            mask = im.open(path_to_UNITY + obj["mask_file"])
            some_name = im.new("RGBA", mask.size, "WHITE")
            some_name.paste(mask, (0, 0), mask)
            some_name = ImageOps.grayscale(some_name)
            some_name = ImageOps.invert(some_name)
            seg = mask_utils.encode(np.asarray(some_name, dtype="uint8", order="F"))
            tmp["segmentation"] = seg
            tmp["bbox_mode"] = BoxMode.XYXY_ABS
            bbox = mask_utils.toBbox(seg)
            bbox = [bbox[0], bbox[1], bbox[0] + bbox[2], bbox[1] + bbox[3]]
            tmp["bbox"] = bbox
            tmp["category_id"] = obj["material_id"]  # chybí překlad na vlastní
            if gen_id:
                tmp["category_id"] = OWN_to_id[obj["material_id"]]
            else:
                tmp["category_id"] = obj["material_id"]
            annotation.append(tmp)
        annot["annotations"] = annotation
        glob_annotation.append(annot)

    return glob_id, glob_annotation


if __name__ == "__main__":
    glob_id = 0
    glob_annotation = []
    (glob_id, glob_annotation) = UNITY_main("Source datasets/UNITY/DICE/", glob_id, glob_annotation, gen_id=False)
    with open('DATASET/annotations.data', 'wb') as file:
        pickle.dump(glob_annotation, file)
