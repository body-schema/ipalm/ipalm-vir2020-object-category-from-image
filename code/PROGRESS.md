### 2021 Summer CTU internship, concerning additional ROI head for the detectron2 pipeline

## WHAT & WHY
We need to separate the object category and the object surface materials because currently the material **ASS**esment (i.e. plastic, wood...) is dependent on the object category (i.e. cup, mug, coffeemaker...).


Github fork of Detectron2 is here for now: https://github.com/Hartvi/detectron2

Datasets:
[BOP felk YCBV](https://bop.felk.cvut.cz/datasets/)

[Shop VRB](https://michaal94.github.io/SHOP-VRB/)

[YCB](https://www.ycbbenchmarks.com/)

[Unity Sponges & Dice](https://drive.google.com/drive/folders/1ujW8lD5FBNhz46CSuK7PueUcd8TJ1oqa?usp=sharing)

## TODOs

- [ ] collect a large material dataset, based on [these papers](https://drive.google.com/drive/u/0/folders/1GYq5U_rNQ2JWd_6he3sI7lVMoGnjZ46M) PERHAPS
- [X] consult the dataset shape and postprocessing with Michal Pliska
- [X] read about the multiple [ROI heads usage here](https://github.com/facebookresearch/detectron2/issues/841)
  - See Technical TODOs (TTs) for details
- [ ] make the net output all of the probabilities, not only the top one (i.e. the top probability is 85% plastic, but we also need the rest of the probabilities (exhaustive) such as 8% wood, 3% steel and so on). This should not be too difficult.
- Three possibilities:
  1. Detectron2 head: too difficult
  2. Detectron2 with segmentor backbone: can't download dataset - gets stuck/disconnects us or something
  3. Material patch classification: seems feasible atm.

#### Detectron2 head
- [ ] **TOO HUGE** a task for the summer: Create new head and train it for material segmentation - Simple *REFERENCE*: [from slide 26 onwards](https://www.dropbox.com/s/o83efjejx1kfrsy/ICCV19_Detectron2.pdf?dl=0)
- [X] **Document** the possible `instances`' and other data's structures for our purposes
  - gt_classes, gt_masks, gt_boxes and now our new gt_materials
- [ ] **Find out** what `gt_classes` does in `material_head.material_rcnn_loss`
  - Is it from the created dataset, does it even have to be there, can we use only gt_materials for loss calculation
- [X] **Find out** how to structure the input data for each head
- [ ] **Find out** how to ignore empty/missing annotations in `forward`
- [ ] **Modify** Michal's scripts to include material masks. Perhaps `gt_material_mask`
  - [X] First **check** if it is only gt_materials_masks that are needed
    - According to panoptic_deeplab's `train_net.py`: no, just add the extra stuff you need
- [X] **Find out** how to handle the `material_rcnn_loss` & `material_rcnn_inference` in the `material_head`
  - This can be done by calculating loss with `gt_material` which can be gained as a member of `instances` when compiling the dataset in the COCO format and in functions such as `json_to_annotations` 
- [ ] **Write** a comment about what I've found regarding adding the heads [here](https://github.com/facebookresearch/detectron2/issues/3321); perhaps add a comment to the issue [here](https://github.com/facebookresearch/detectron2/discussions/2559) or [here](https://github.com/facebookresearch/detectron2/issues/2944) regarding `RuntimeError: Expected to have finished reduction in the prior iteration`

### Patch material
- [X] check data - there were 2 black and white images 
- [ ] GET `custom_vis.py` to work. **Error**:
```Traceback (most recent call last):
  File "custom_vis.py", line 95, in <module>
    predictor = DefaultPredictor(cfg)
  ...
RuntimeError: storage has wrong size: expected 0 got 256
```
- [ ] get the output masks/bboxes out of detectron inference and plug them into the mobilenet

## PROGRESS

### 2021/09/16-17
- training the net on even more augmented data
- script now outputs selected classes [materials that we want] and has a threshold setting above which the materials with high probabilities will be displayed

### 2021/09/14-15
- downloaded [mobilenetv3](https://github.com/dkumazaw/mobilenetv3-pytorch/)
- minc-2500 dataset contains 2 black and white images which were crashing the training because of the wrong number of channels
- trained the mobilenet it on the MINC dataset
- see code/patch_material_stuff???/*.py for the scripts


### 2021/09/10-13
- trying to download the MINC dataset from the cornell univeristy website - GETS STUCK/DISCONNECTS us every time
- while that is going on, we're trying to train a mobilenetv3 material patch classification network based on the smaller 2.1GB dataset from cornell opensurfaces - is throwing some errors: some data might be faulty, gotta filter it out
- **ERROR**:\
  imgg = img.transpose((2, 0, 1)) / 255.0\
  ValueError: axes don't match array


### 2021/09/01
- **REVAMP**: run a separate semantic segmentation network (Deeplab or pointrend for semantic segmentation), then extract the instance masks from the VIR network; get the `pred_masks` then find out which materials have been predicted there by the semantic segmentation net
- Dataset tutorial: https://detectron2.readthedocs.io/en/latest/tutorials/datasets.html
- Model output format: https://detectron2.readthedocs.io/en/latest/tutorials/models.html#model-output-format
<img src="https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F6b6c6fec-af90-4f34-a11b-bc4325248a47_1418x1004.png" alt="detectron2 architecture" width="400">

### 2021/08/31
- Added `projects/ipalm/ipalm/detection_utils` and implemented minimal support for the material head; compare with the regular `detection_utils`
- Added `projects/ipalm/ipalm/dataset_mapper` and again created minimal material head support
- BIG progress, if there is no `material_id` and no `material_segmentation` in the input annotations, then dummy annotations are created with values -1 or material mask size (0, 0) 
  - the material mask of size (0, 0) is detected in `materialhead/material_rcnn_loss()` and returns a backprop-compatible loss of 0 which leads to a zero gradient, thus not learning when there are no proper masks present
  - TODO the same for regular masks when there is no proper `gt_classes` and `gt_masks`

### 2021/08/30
- `detectron2/data/datasets/coco.py`: `def load_coco_json(json_file, image_root, dataset_name=None, extra_annotation_keys=None):`
  - in `extra_annotation_keys` enter material as an extra field
  - It is the same logic as `projects/PointSup/point_sup/register_point_annotations.py`: `def register_coco_instances_with_points(name, metadata, json_file, image_root):`
- `gt_masks`, `gt_boxes`, `gt_keypoints`, `gt_classes` are created in `detectron2/data/detection_utils.py`: `def annotations_to_instances(annos, image_size, mask_format="polygon"):`
- use `point_head` and `point_utils` as reference in comparison with the detectron2 `mask_head` and `detection_utils`
- continue from `dataset_mapper:136` find out where exactly to plug in the `gt_materials` field

### 2021/08/25
- To find out what to change in order to train with different architectures, compare two different `train_net.py` files from anywhere
- Overview of what needs to be changed to support the extra material head:
  - Add `gt_material` to `instances`
  - Add `data/` `annotations_to_instances` & maybe `instances_to_annotations`
  - Custom dataset evaluator similar to the `detectron2/evaluation/COCOEvaluator` from vanilla detectron
  - Possibly load `gt_material` with extra arguments in `datasets/coco.py/load_coco_json()`
- Use [MeshRCNN](https://github.com/facebookresearch/meshrcnn/) as reference for building the thing

### 2021/08/24
- To run detectron2 while not having the proper data for all 3 heads, it is necessary to set `find_unused_parameters=True` inside the trainer => this will **ignore** that there isn't a loss computed for the material head as well as slow down training
  - Find unused parameters is set inside the default trainer `__init__` as an argument: `create_dpp_model(find_unused_parameters=True)`, found out from the name "[ddp](https://pytorch.org/docs/stable/generated/torch.nn.parallel.DistributedDataParallel.html)"
- **FOUND OUT** how to MOUNT the school GPU server user drive on windows using: https://www.nsoftware.com/sftp/drive/
- Found out that the loss for the extra head has to be calculated in a custom function that takes a new field in the `instances` called, let's say, `gt_material_mask` and maybe more => todo

### 2021/08/23
- The error that gets thrown when running detectron2 with the extra head is the same that people  are seeing here:
  - **[RuntimeError:Expected to have finished reduction in the prior iteration before starting a new one #2153 ](https://github.com/open-mmlab/mmdetection/issues/2153#issuecomment-706481853)**
  - Related issue: [RuntimeError: Expected to have finished reduction in the prior iteration before starting a new one. #43259](https://github.com/pytorch/pytorch/issues/43259)
  - It is an unsolved issue on the detectron2 issue hub: [use 2 gpus to train the modified network #2944](https://github.com/facebookresearch/detectron2/issues/2944)
- On a positive note, we've got Michal's code for the dataset generation which needs detectron2, pytorch, pycocotools and other packages to be installed beforehand
  - The script from Michal works ok - for computers without cuda it is good to use the pytorch cpu version => [link](https://pytorch.org/get-started/locally/)
  - On the current box PC we're using the conda (base) environment with all the packages

### 2021/08/19
- Adding new head. Files I've changed/added in roughly the order that I discovered what had to be changed:
  - **Added** `detectron2/configs/COCO-InstanceSegmentation/mask_rcnn_ipalm.yaml` - modified copy of `mask_rcnn_R_50_FPN_3x.yaml`
  - **Added** `detectron2/configs/Base-ipalm.yaml` - modified copy of `Base-RCNN-FPN.yaml`
  - **Added** `detectron2/projects/ipalm/ipalm/material_head.py` - contains `MaterialHead` - new head for material masks. Basically a copy of `MaskRCNNConvUpsampleHead` from `detectron2/detectron2/modeling/roi_heads/mask_head.py`. May be modified later.
  - **Added** `detectron2/detectron2/modeling/roi_heads/ipalm_heads.py` - contains `IpalmROIHeads` - custom `ROIHeads` implementation for bbox, mask and material mask. Basically a copy of `StandardROIHeads` from `detectron2/detectron2/modeling/roi_heads/roi_heads.py` with everything that contains mask/MASK copied with material/MATERIAL instead
  - **Edited** `detectron2/detectron2/modeling/roi_heads/__init__.py` - added the new imports from above
  - **Edited** `detectron2/detectron2/modeling/__init__.py` - added the new imports again
  - **Added** `detectron2/projects/ipalm/config/config.py` - added `add_material_head_config()` this has to be called before `cfg.merge_from_file()` because that expects keys to exist for some reason
1. Adding new head: after changing name of the orignal .yaml file in `train.py`:<br>
  `mask_rcnn_R_50_FPN_3x.yaml` => `mask_rcnn_ipalm.yaml`<br>
  `cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))`<br>
  | |<br>
  \\/<br>
  `cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_ipalm.yaml"))` <br>
  This causes errors such as `KeyValueError: missing key: MODEL.MATERIAL_HEAD`<br>
2. Registering a new ROIHeads: `ValueError: Missing object: MODEL.IpalmROIHeads`
    - To register a class inheriting either `ROIHeads`/`StandardROIHeads`/others:
      - rn it seems that the `ipalm_heads.py` containing `IpalmROIHeads` has to be in the same directory as `roi_heads.py` - `detectron2/detectron2/modeling/roi_heads/`
3. Training with the new `IpalmROIHeads`: `RuntimeError: ... parameters not used in producing loss`
  - **TODO**: Need a custom dataloader or datamapper or something to calculate loss even when there is an empty *mask+bbox* or *material mask*
    - <img src="https://i.imgur.com/VN7CAUL.png" alt="New dataset annotations" width="250">

### 2021/08/17
- found out that add_material_head_config(cfg) has to be called before merging from file in `train.py`; otherwise as above
  - `missing key: MODEL.MATERIAL_HEAD` error might be caused by this

### 2021/08/16
- StandardROIHeads -> IpalmHeads - modify forward to ignore empty masks/material/bbox
- MaterialHead - mostly the same as the mask head to be used for material training data in IpalmHeads
- use custom dataloader, custom dataset and see documentation for writing models

### 2021/08/12
- Building multiple ROI heads:
  - [Multiple ROI heads for predicting multiple attributes as in bottom-up attention #841](https://github.com/facebookresearch/detectron2/issues/841)
- How to train detectron2 with many heads: 
<img src="https://i.imgur.com/bhnetJH.png" alt="detectron2 architecture" width="400">

- Issues pertaining to multiple head training:
  1. **[Training with different level of supervisions #45](https://github.com/facebookresearch/detectron2/issues/45)**
  - [Keypoint + segmentation mask in Detecton2 #2871](https://github.com/facebookresearch/detectron2/discussions/2871)
  - [How to enable both mask and keypoint in your config ? #1219](https://github.com/facebookresearch/detectron2/issues/1219)
  - [mutil task training #17](https://github.com/facebookresearch/detectron2/issues/17)
  - [cannot train detection and instance segmentation together with Mask-RCNN #1190](https://github.com/facebookresearch/detectron2/issues/1190)
  - [Training keypoints + detection heads jointly #276](https://github.com/facebookresearch/detectron2/issues/276)
- Documentation related to the issues:
  - [Use Custom Datasets](https://detectron2.readthedocs.io/en/latest/tutorials/datasets.html)
  - Writing custom models: [Write Models](https://detectron2.readthedocs.io/en/latest/tutorials/write-models.html)
  - Losses are calculated in FasterRCNN stuff: [FastRCNNOutputLayers](https://detectron2.readthedocs.io/en/latest/modules/modeling.html#detectron2.modeling.FastRCNNOutputLayers)
  - For different formats of datasets there are needed custom data loaders: [Write a Custom Dataloader](https://detectron2.readthedocs.io/en/latest/tutorials/data_loading.html?highlight=dataloader#write-a-custom-dataloader)
- Marginally related:
  - [Is it possible to train with multiple datasets? #2544](https://github.com/facebookresearch/detectron2/issues/2544)
  - [How to train a pretrained model on image with 6 input channels(2 concatenated RGBs)? #2062](https://github.com/facebookresearch/detectron2/issues/2062)
  - [Continuing training with a different config #429](https://github.com/facebookresearch/detectron2/issues/429)
- Unrelated to training heads, but to other things closer to our case:
  - [How to train Detectron2: Instance Segmentation (R101-FPN 3x) with custom dataset #2856](https://github.com/facebookresearch/detectron2/discussions/2856)
  - [Registering custom dataset that is in the COCO format #2737
](https://github.com/facebookresearch/detectron2/discussions/2737)
  - [Hide / Remove bounding Box at Inference #2992](https://github.com/facebookresearch/detectron2/discussions/2992)
  - [How to get bbox from segmentation? #2504](https://github.com/facebookresearch/detectron2/discussions/2504)
  - [How to set up other coco dataset path? #2677](https://github.com/facebookresearch/detectron2/discussions/2677)
  - [Mask RCNN dataset labelling #2577](https://github.com/facebookresearch/detectron2/discussions/2577)
- Das [Fazit](https://www.dict.cc/german-english/Fazit.html):
  1. Combine the existing dataset with the MINC dataset
  2. Training the network: 
      - In the VIR part, mask and bbox are as is and the material mask is empty in the annotation
      - Alternately in the MINC part, the mask and bbox are empty and the material mask is filled
      - This should be possible according to issue #45

### 2021/08/11
- When copying big files on the server to the user's directory, make sure all the images are copied. Training might fail with missing images
- What to change:
  - detectron2/config/Base-ipalm.yaml is to contain the new material head 
  - detectron2/config/mask_rcnn_ipalm.yaml - new specific yaml
  - inherit detectron2/detectron2/modeling/roiheads/StandardROIHeads.py and add another MaskRCNNConvUpsampleHead from mask_head.py
- TODO: investigate [forking](https://docs.github.com/en/get-started/quickstart/fork-a-repo) the Detectron2 from github? 
- Surface material science [articles](https://drive.google.com/drive/u/3/folders/1GYq5U_rNQ2JWd_6he3sI7lVMoGnjZ46M)
  - Materials in context database ([MINC](http://opensurfaces.cs.cornell.edu/publications/minc/))

### 2021/08/10
- To run detectron 2:
  1. Have dataset in [this format](https://drive.google.com/drive/u/3/folders/1KTq8mnnkVKlvqy9VSHHRTv0BhWU8-vE-)
  2. Absolute path of datasets in train.py has to match the path of DATASET from the point above
      - If there is no local weight checkpoint, then `cfg.MODEL.WEIGHTS` is to be loaded online
  3. On school servers: `ssh username@cantor.felk.cvut.cz` => kos password
  4. DATASET for now has to be in the same directory as train.py
  5. In terminal run:
      - `ml torchvision` => this should load torch >=1.7.0 and torch vision and coco loader, etc as well.
        - Just in case the needed packages are: `PyTorch/1.8.0-fosscuda-2019b-Python-3.7.4` and `torchvision/0.9.1-fosscuda-2019b-PyTorch-1.8.0`
      - `train.py --num-gpus number`
- To edit on server from windows use [MobaXterm](https://mobaxterm.mobatek.net/) and insert [.vimrc](https://gist.github.com/miguelgrinberg/527bb5a400791f89b3c4da4bd61222e4#file-vimrc-L3)

### 2021/08/09
- Found overview of the bounding box head [Digging into detectron2](https://medium.com/@hirotoschwert/digging-into-detectron-2-part-5-6e220d762f9)
- Added comment to issue that has the same problem as us: [#3321](https://github.com/facebookresearch/detectron2/issues/3321)

