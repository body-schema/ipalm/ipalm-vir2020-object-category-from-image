# Object Category & Material Inference

Below, you can see a showcase of our results on some household and lab objects.

![](gifs/test_results_crop.gif) ![](gifs/household_crop.gif)


The goal of this project was to train a well-known convolutional neural network (Mask R-CNN) on a new dataset and experiment also with multi-label classification - the desired output are object category and material labels. 

# REPORT 

You can check our online report [here](https://prezi.com/i/rrf1sj4fgrxr/).


# ipalm-vir2020-object-category-from-image
Google Docs: [VIR 2020 - Object categories from image](https://docs.google.com/document/d/10-9w-OJ1-2zrLMKeVs_aJn1XcUWU_XWnOaRn8IUojTM/edit)
<br>
Google Drive: [VIR 2020 - Object categories](https://drive.google.com/drive/u/1/folders/1GDM4l2QTVPstgSI4_5ma9hA442WcziML)

How to [Setup Anaconda environment on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-anaconda-on-ubuntu-18-04-quickstart)

# Datasets
SHOP-VRB Dataset can be found [here](https://michaal94.github.io/SHOP-VRB/)

YCB Dataset can be found [here](http://ycb-benchmarks.s3-website-us-east-1.amazonaws.com/)

Generated YCB dataset can be found [here](https://bop.felk.cvut.cz/datasets/)


All informations about our dataset are [here](https://docs.google.com/document/d/1CW4FBL0n4Ri0rGImGer7ghMOOkfN_zMKOTT_n5or8pA/edit)
(direct link to [folder](https://drive.google.com/drive/folders/1ozhBJdDq9hpJn6fDXAqIUmcJ9379z_Rj))


# Detectron2, initial setup
- modules loaded are stored in:`.bash_profile`
as of 3/12/2020: ml PyTorch/1.5.0-fosscuda-2019b-Python-3.7.4 (nvcc release: 10.1; PyTorch version: 1.5)
- running currently release v0.3 Detectron2 ([link here](https://github.com/facebookresearch/detectron2/releases/tag/v0.3))

4/12/2020
- the prebuilt image is probably only good for API stuff and using everything online, so I've
cloned latest git repo of detectron2 and installed it locally via:\
`git clone https://github.com/facebookresearch/detectron2.git` \
`python -m pip install -e detectron2 --user` 
- Also needed to call: \
-`pip install opencv-python --user` because opencv in **modules** comes only with different python, torch, and CUDA versions. So to **NOT MESS EVERYTHING UP** I chose to install it again locally. *(I'm sorry, Cantor)*
- If the above doesn't work: use `ml torchvision/0.9.1-fosscuda-2019b-PyTorch-1.8.0 && ml OpenCV/3.4.8-fosscuda-2019b-Python-3.7.4` every time when starting up on the server
  - In addition, the pytorch that gets loaded along with torchvision should be version `PyTorch/1.8.0-fosscuda-2019b-Python-3.7.4` as of 2021/09/17
- demo has been run succesfully. **congrats**

# Final Version 

## setup
All code neede is in the `/code` directory.
After you have succesfully installed detectron2 library, you will need to setup your dataset. (@cantor/taylor at `/local/temporary/`)
- if you cannot store your DATASET locally next to the `train.py` (i.e. need to store dataset in some other directory), you will need to edit and run the  `path_correction.py` to edit all of the annotation files inside the `train.data`/`val.data`/`test.data`

There is actually no need to keep a separate test/validation dataset, but we did it anyway.
After you've changed the local paths in the annotations via `path_correction.py` you are ready to start the training.

## train with train.py
We've made a custom trainer, *(MegatronTrainer)* that fits our needs. You are free to customize it as you see fit, there are plenty of ways to utilize the detectron2's modular design.
### 1) PATHS
Change the paths for the dictionaries `id_to_OWN.json`, `train.data` and `val.data`, and also for the datasets `val_data` and `trn_data` to match your dataset locations.
    - the code will create corresponding list for METADATA to add to the dataset registration (for visualisation)
### 2) DATASETS
The code will next register the Datasets (their names are the paths `trn_data` and `val_data`) and provide corresponding metadata
### 3) HYPERPARAMETERS
Setup the HYPERPARAMETERS and other PARAMETERS in the `setup()`
    - the training with detectron is iteration (not epoch) oriented, but you can compute the number of epochs as (`cfg.SOLVER.MAX_ITER` / `cfg.TEST.EVAL_PERIOD`) to be the number of epochs planned.
    - `cfg.SOLVER.STEPS` is number after which will the `cfg.SOLVER.BASE_LR` be multiplied by `cfg.SOLVER.GAMMA`
    - for complete list of configurable parameters see [detectron2 documentation](https://detectron2.readthedocs.io/modules/config.html#config-references)
    - it is possible (and also recommended) to start with some **Baseline Weights** from the [model_zoo](https://github.com/facebookresearch/detectron2/blob/master/MODEL_ZOO.md) or your previous training
### 4) START THE TRAINING
Call `python train.py --num-gpus NUM_GPUS` if you want to run on multiple GPUS
    - make sure to set up `cfg.SOLVER.IMS_PER_BATCH` to be a multiple of the `NUM_GPUS` for the workers to be able to split the workload between the GPUs
    - it may be useful to make only completely free GPUs visible beforehand (to reduce the chance of running out of shared memory, if you accidentally connect to someone elses GPU) with `export CUDA_VISIBLE_DEVICES=NUM_OF_GPU1,NUM_OF_GPU2,...,NUM_OF_GPUn,` 
    - if you trained before and reached some checkpoint and would like to resume the training from a snapshot of the model with checkpointed weights and all of the local gradients and so on you can add a flag just right behind the file as follows: `python train.py --resume --num-gpus NUM_GPUS` (this means, any changes in the `setup()` will be ignored)

### 5) INFERENCE & TESTING
In order to see the results of your CNN, you can ran the visualistaion script `/code/final/custom_vis.py`.
In order to change the parameters, you will need to change paths in the `custom_vis.py` to fit your files.
The video visualisation is still broken. This issue may be resolved in the future.
If you insist on producing some video output (as we did in our report), you can run the inference frame by frame*.
The code is thoroughly commented, so there should be no problem with editing. :)
After you're all set, run `python3 custom_vis.py` and you are good to go.

PS: The inference was run on GPUs with CUDA for speed. Running it on CPU may take a bit longer.
PS2: There may be an update for the inference, that would allow just using arguments as input, so there'd be no need for code editing...

*the video visualisation software we tried to use in the `custom_vis.py` should to the same thing > disassemble the video to frames, run prediction and assemble predictions back to video format. However, it is a thirdparty software, that is not very transparent. It is recommended you do the video prediction manualy. For video to frames decomposition, you can use various online tools.

## 6) Issues - from fool to god
- `python train.py` fails => try the next line:
- `python train.py --num-gpus number1-8` fails => try different numbers
- `train.py` - can't find `DATASET/` => 
  1. Set the absolute of path `DATASET/` correctly in all instances in `train.py`
  2. `DATASET/` and `train.py` have to be in the same directory
- `Importerror: cannot import name '_C'` [#157](https://github.com/facebookresearch/detectron2/issues/157) => build detectron again from directory where the highest `detectron2/` is located, see [Detectron2, initial setup](# Detectron2, initial setup) at the top on how to build Detectron2


# Acknowledgements
This work was supported by the project Interactive Perception-Action-Learning for Modelling Objects (IPALM, https://sites.google.com/view/ipalm) (H2020 -  FET - ERA-NET Cofund - CHIST-ERA III / Technology Agency of the Czech Republic, EPSILON, no. TH05020001).  

<p align="middle">
 <img src="./tacr.png" width=10%>
</p>  


### CTU, FEE 2021: Hartvich Jiri, Kruzliak Andrej, Pliska Michal


