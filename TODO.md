# TODO

## MAJOR MILESTONES
- [X] - First working version of RCNN
- [X] - First trained version of RCNN
- [X] - Joined datasets, with additional scenes generated in Blender/Unity
- [X] - Semantic inference of "something" on the picture
- [X] - Bounding box of that "something" on the picture
- [X] - Material inference of that "something" on the picture
- [ ] - Separated ROI heads for independent Object and Material inference
- [ ] - Better Performance (smaller loss, better AP and IOU -> more training)
- [ ] - Created Presentation with videos
# Smaller TODOs
- [ ] - collect common information about stuff that detectron2 uses
  - optimizer, evaluator, gradient clipping, lr_scheduler, number of GPUs for training, api documentation, possibly a replication tutorial, dataset formatting, mix of real/CGI pictures
  - **Michal**
    - summarize info on dataset creation (how and why, what were the challanges, problems, and propose possible improvements)
    - thoroughly comment your created code, so its idiot-proof, and make it available for the rest of the team
    - link all materials used in the process (or at least the ones you can remember :D )
  - **Jiri**
    - summarize info on scene creation in Unity (how and why, what were the challenges, problems, and propose possible improvements) [Document](https://docs.google.com/document/d/1mpZuIglBlU4H_W2vgSlnljUoq4cr36DqdpnIrMB1m1s/edit)
    - thoroughly comment your created code, so its *idiot-proof*, and make it available for the rest of the team [info@UnityScene](https://gitlab.fel.cvut.cz/body-schema/ipalm/ipalm-vir2020-object-category-from-image/-/blob/master/UnityScene.md), [GitHub](https://github.com/Hartvi/ipalm-sponges)
    - in short steps, provide a tutorial on how to create a scene in unity (only in keywords --> 1) install Unity and other needed 
  software (specify the stuff Nazarczuk recommended), 2) create 3d objects, do this, do that, 3) shoot the lasers 4) create RLE or what and so on...) [Summary](https://docs.google.com/document/d/1mpZuIglBlU4H_W2vgSlnljUoq4cr36DqdpnIrMB1m1s/edit#heading=h.bgf3319ysi3t)
    - show some pictures, also a gif or short screencapture from the creation process would be great (for example hide all layers and shaders and then start to uncover layers and shaders one by one, so effects start appearing) -> veri coooool for presentation :D [here you go](https://docs.google.com/document/d/1mpZuIglBlU4H_W2vgSlnljUoq4cr36DqdpnIrMB1m1s/edit#heading=h.pb18aukhl3t6)
  - **Andrej**
    - summarize info on MaskRCNN and why we chose detectron2, summarize info on detectron2
    - thoroughly comment your created code, so its idiot-proof, and make it available for the rest of the team
    - show some metrics and stuff, overall loss, class loss, how the LR changed in the process and so on **(dont forget to hide loss meme into the presentation)**
    - summarize used APIs, show code, show trainer, comment on hihg-levelness of the detectron2 API, comment on the thorough documentation
    - create a nice presentation in PREZI or other coooool sw **(god forbid using powerpoint!)**
    - possibly incorporate other memes, because memes bring likes, memes bring points, memes bring A's
- [ ] - record inference video on household items **THE ULTIMATE TEST**
- [ ] - Create a nice way to save logged data
- [ ] - Create a nice way to visualize the logged data
- [ ] - Collect sponges from the [OpenSurfaces](http://opensurfaces.cs.cornell.edu/shapes/substance/all/45/)
- [X] - Take photos of sponges and dies or/and create scenes with sponges and dies in Blender/Unity
- [X] - Standardize *object names* and *material properties*. E.g. (sponge, plastic). 
    - Maybe create JSON mappings like in SHOP_VRB
    - **Google Docs [proposal](https://docs.google.com/document/d/17A0UmGHt5yyQ2yHSfWNWVWTgazwPiEa8kaEZiqwpz0U/edit?usp=sharing)**
- [X] - Translate all used labels also from other datasets into the *(thing:material)* form
    - for every object from the dataset we "want" a label representation as follows: (mug,ceramic) - first label; (mug, plastic) - second label and so on...
- [X] - be inspired by the SHOP_VRB scenes on how to form the .json file for OUR dataset
- [X] - count all the occurances of all object accross all datasets
    - so we have a clue how well balanced the dataset is and how many sponges we need to generate
- [X] - **URGENT** resolve the RLE problem (for example pycocotools expect `image_id` to be also in the 'segmentation' part of the 
`annotations.json`)
- [X] - `git` the shit out of this whole repo
- [X] - Reformat all datasets into one big dataset 1) bounding boxes, 2) mask in COCO style (Common Object in COntext) 
- [X] - Make overview of SHOP-VRB dataset and (object, material) categories within 
- [X] - Make overview of YCB(Fel) dataset and (object, material) categories within 
- [X] - Point out that all datasets are generated and not photographed!
- [X] - Share really simple code in Python, which shows how to work with json files
- [X] - Summarize all existing categories and propose new for own dataset
- [X] - read about RLE (Run Length Encodign), BinaryMaskList, in `segmentation_mask.py`
- [X] - WHO IS COCO? WHAT IS COCO? WHY IS COCO?
- [X] - Check HW specs we can use for the SW on Cantor, taylor
 - 500 GB per server, not per student - *should be OK for file keeping up to 10GB for example*
 - most of the modules seem to be installed @taylor/cantor, but not sure about `cython`
    - `install ninja !yacs! !cython! matplotlib tqdm opencv-python`
    > not needed - check how the `docker` works
    - install packages @cantor/taylor with `pip install "package" --user`, or use conda
- [X] - Create TODO
> DEPRECATED: - Download dataset from the RCNN Paper
    - and try it out on the taylor/cantor

Some free datasets of different objects from the web (original images for non-commercial use only):
[**50 free datasets**](https://blog.cambridgespark.com/50-free-machine-learning-datasets-image-datasets-241852b03b49)

Familiarize yourself with the Mask-RCNN.
- Prepare the dataset - SHOP-VRB + YCB + “cubes, dies, sponges”.
- For “cubes, dies, sponges”, there are these options:
    - Take photos in our lab (E210).
        - Model cubes etc. in blender and take pictures there.
        - Use samples from http://opensurfaces.cs.cornell.edu/ (e.g., foams, http://opensurfaces.cs.cornell.edu/shapes/substance/all/45/) 
        - Prepare the labels - object categories and material labels + dataset structure - training, validation, testing - see SHOP-VRB
- Train the network.
    - Combine labels into tuples [category, material].
    - Train two separate networks for category and material.
    - [Multi-label classification network] 
- Test network on new objects.

Output:
- Object categories: There are 20 in SHOP-VRB (Baking tray, Blender, Bowl, Chopping board, Coffee maker, Food box, Fork, Glass, Kettle, Knife, Mug, Pan, Plate, Pot, Scissors, Soda can, Spoon, Thermos, Toaster, Wine glass). 
    - Modify these to **take into account** also the YCB and “Cubes, dies, sponges”.
- Material labels: Use SHOP-VRB [rubber,metal,plastic,wood,ceramic,glass] or extend as we extend the dataset (adding e.g., foam). 
    - To account for “cubes, dies, and sponges”, let us **split plastic** into “soft plastic” and “hard plastic” and let’s **add** “foam” (as porous polyurethane foam).


# Error messages

```
-- Process 1 terminated with the following error:
Traceback (most recent call last):
  File "/home.others/eb/easybuild/software/PyTorch/1.5.0-fosscuda-2019b-Python-3.7.4/lib/python3.7/site-packages/torch/multiprocessing/spawn.py", line 20, in _wrap
    fn(i, *args)
  File "/home.nfs/kruzland/detectron2/detectron2/engine/launch.py", line 94, in _distributed_worker
    main_func(*args)
  File "/home.nfs/kruzland/detectron2/demo/version_2/train.py", line 259, in main
    return trainer.train()
  File "/home.nfs/kruzland/detectron2/detectron2/engine/defaults.py", line 419, in train
    super().train(self.start_iter, self.max_iter)
  File "/home.nfs/kruzland/detectron2/detectron2/engine/train_loop.py", line 144, in train
    self.after_train()
  File "/home.nfs/kruzland/detectron2/detectron2/engine/train_loop.py", line 153, in after_train
    h.after_train()
  File "/home.nfs/kruzland/detectron2/detectron2/engine/hooks.py", line 361, in after_train
    self._do_eval()
  File "/home.nfs/kruzland/detectron2/detectron2/engine/hooks.py", line 331, in _do_eval
    results = self._func()
  File "/home.nfs/kruzland/detectron2/detectron2/engine/defaults.py", line 372, in test_and_save_results
    self._last_eval_results = self.test(self.cfg, self.model)
  File "/home.nfs/kruzland/detectron2/detectron2/engine/defaults.py", line 525, in test
    data_loader = cls.build_test_loader(cfg, dataset_name)
  File "/home.nfs/kruzland/detectron2/detectron2/engine/defaults.py", line 484, in build_test_loader
    return build_detection_test_loader(cfg, dataset_name)
  File "/home.nfs/kruzland/detectron2/detectron2/config/config.py", line 201, in wrapped
    explicit_args = _get_args_from_config(from_config, *args, **kwargs)
  File "/home.nfs/kruzland/detectron2/detectron2/config/config.py", line 236, in _get_args_from_config
    ret = from_config_func(*args, **kwargs)
  File "/home.nfs/kruzland/detectron2/detectron2/data/build.py", line 399, in _test_loader_from_config
    else None,
  File "/home.nfs/kruzland/detectron2/detectron2/data/build.py", line 222, in get_detection_dataset_dicts
    dataset_dicts = [DatasetCatalog.get(dataset_name) for dataset_name in dataset_names]
  File "/home.nfs/kruzland/detectron2/detectron2/data/build.py", line 222, in <listcomp>
    dataset_dicts = [DatasetCatalog.get(dataset_name) for dataset_name in dataset_names]
  File "/home.nfs/kruzland/detectron2/detectron2/data/catalog.py", line 58, in get
    return f()
  File "/home.nfs/kruzland/detectron2/detectron2/data/datasets/coco.py", line 470, in <lambda>
    DatasetCatalog.register(name, lambda: load_coco_json(json_file, image_root, name))
  File "/home.nfs/kruzland/detectron2/detectron2/data/datasets/coco.py", line 60, in load_coco_json
    coco_api = COCO(json_file)
  File "/home.nfs/kruzland/.local/lib/python3.7/site-packages/pycocotools/coco.py", line 83, in __init__
    with open(annotation_file, 'r') as f:
FileNotFoundError: [Errno 2] No such file or directory: 'datasets/coco/annotations/instances_val2017.json'
```

